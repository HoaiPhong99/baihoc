import React from 'react';
import PropTypes from 'prop-types';

const ChildComponents = props => {
    // console.log("props", props.desc);
    return (
        <div>
            Name: {props.name} <br/>
            Price: {props.price} <br/>
            Description: {props.desc} 
        </div>
    );
};

ChildComponents.propTypes = {
    
};

export default ChildComponents;