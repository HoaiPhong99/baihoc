import Loadable from 'react-loadable'
import Loading from './Pages/Loading'

export const AsyncHome = Loadable({
    loader: () => import('./Pages/Home'),
    loading: Loading,
})

export const AsyncUser = Loadable({
    loader: () => import('./Pages/Users'),
    loading: Loading,
})

export const AsyncContact = Loadable({
    loader: () => import('./Pages/Contact'),
    loading: Loading,
})

export const AsyncUserDetail = Loadable({
    loader: () => import('./Pages/UserDetail'),
    loading: Loading,
})

export const AsyncTopics = Loadable({
    loader: () => import('./Pages/Topics'),
    loading: Loading,
});

export const AsyncReactiveComponents = Loadable({
    loader: () => import('./Pages/ReactiveComponents'),
    loading: Loading,
});

