import React, {useState} from 'react';
import PropTypes from 'prop-types';
import ChildComponents from '../../Components/ChildComponents';
import PopupComponets from '../../Components/PopupComponents';


const ReactiveComponents = props => {

    const [count, setCount] = useState(0);
    const [var1, setVar1] = useState({
        a:0
    });
    const [type, setType] = useState('add');
    const [showPopup, togglePopup] = useState(false)
    // console.log('var1', var1.a);
    // const handleSetVar1 = () => console.log('var1: ', var1.a);
    // const handleSetVar1 = () => {
    //     let newVar1 = {
    //         ...var1,
    //         a: var1.a + 1,
    //         b:3
    //     }
    //     setVar1(newVar1);
    // };
    const handleSubmit = (params1) => {
        console.log('abs', params1);
    }
    return (
        <div>
            {/* <p>You clicked {count} times</p> */}
            {/* <p>You var1 {var1} times</p> */}
            {/* <button onClick={() => setCount(count + 1)}>
                Click me
            </button> */}
            {/* <button onClick={() => handleSetVar1()}>
                Click me
            </button> */}
            <button onClick={() => togglePopup(!showPopup)}>
                Click me
            </button>
            {showPopup?<PopupComponets type={type} handleSubmit={handleSubmit} />:''}
            <hr/>
            <ChildComponents name="Iphone X" price={30000} desc="This is my Iphone X's description"/> <br />
            <ChildComponents name="Iphone Xxx" price={10} desc="asdfadsfsadf"/><br />
            <ChildComponents name="Iphone T_T" price={12} desc="Hi Hi"/><br />

            
            
        </div>
    );
};

ReactiveComponents.propTypes = {
    
};

export default ReactiveComponents;
