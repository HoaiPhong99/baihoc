import React from 'react';
import PropTypes from 'prop-types';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    useRouteMatch
  } from "react-router-dom";


const Topics = props => {
    let { path, url } = useRouteMatch();
    return (
        <div>
            <h2>Topics</h2>
            <ul>
                <li>
                    <Link to={`${url}/renderring`}>Rendering</Link>
                </li>
                <li>
                    <Link to={`${url}/cuccu`}>Cuc cu</Link>
                </li>
            </ul>
            <Switch>
                <Route exact path={path}>
                    <h3>Please select a topic</h3>
                </Route>
                <Route path={`${path}/:topId`}>
                    <Topic />
                </Route>
            </Switch>
        </div>
    );
};


function Topic() {
    let {topId} = useParams();

    return (
        <div>
            <h3>{topId}</h3>
        </div>
    );
} 

Topics.propTypes = {
    
};

export default Topics;