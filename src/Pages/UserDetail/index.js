import React from 'react';
import PropTypes from 'prop-types';
import {
    BrowserRouter as Router,
    Link,
    useParams,
    useLocation,
    useHistory,
    withRouter
} from "react-router-dom";


function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const UserDetail = props => {
    let { id } = useParams();
    let location = useLocation();
    let query = useQuery();
    
    const handleClick = () => {
        let paramName = query.get('name')
        if(paramName === 'home') {
            props.history.push('/');
        } else {
            console.log('Dung im ne`');
        }
    }
    return (
        <>
            <div>
                <h3>ID: {id}, Query: {query.get('name')}</h3>
            </div>
            <button onClick={()=>handleClick()}>ok</button>
        </>

    );
};

UserDetail.propTypes = {

};


export default withRouter(UserDetail);
// export default UserDetail;
