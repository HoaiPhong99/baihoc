import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useLocation
} from "react-router-dom";
// import routes from 'routes.js';
import Contact from './Pages/Contact';
import Users from './Pages/Users';
import Home from './Pages/Home';
import Topics from './Pages/Topics';
import * as routes from './routes';

function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/contact">Contact</Link>
            </li>
            <li>
              <Link to="/users">User</Link>
            </li>
            <li>
              <Link to="/topics">Topics</Link>
            </li>
            <li>
              <Link to="/reactive-components">Reactive Components</Link>
            </li>
          </ul>
        </nav>

        <Switch>
          <Route exact path="/contact" component={routes.AsyncContact} />
          <Route exact path="/users" component={routes.AsyncUser} />
          <Route exact path="/users/:id" component={routes.AsyncUserDetail} />
          <Route exact path="/" component={routes.AsyncHome} />
          <Route path="/topics" component={routes.AsyncTopics} />
          <Route path="/reactive-components" component={routes.AsyncReactiveComponents} />
          <Route path="*">
            <NoMatch />
          </Route>
        </Switch>

      </div>
    </Router>
  );
}

function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match <code>{location.pathname}</code>
      </h3>
    </div>
  );
}


export default App;
